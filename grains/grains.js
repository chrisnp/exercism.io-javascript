const bigInt = require('./big-integer')

class Grains {

    constructor () {}

    square (x) {
        return bigInt(2).pow(x-1).toString()
    }

    total () {
        return bigInt(2).pow(64).prev().toString()
    }

}

module.exports = Grains