const Constants = require('./constants')

class FoodChain {
    
    constructor () {}

    firstLine (verseNum) {

        let output = ''
        if (verseNum >= Constants.MIN && verseNum < Constants.MAX) {
            output = Constants.FIRST_LINE + ' ' + Constants.ANIMALS[verseNum - 1] + '.\n'
            if (verseNum === Constants.MIN + 1) {
                output = output += 'It '  + Constants.SPIDER + '.\n'
            }
        } else if (verseNum === Constants.MAX) {
            output = ''
        } 
        return output
    }

    secondLine (verseNum) {

        if (verseNum === 1 || verseNum === 2 || verseNum === 8) {
            return ''
        } else if ( verseNum >= 3 && verseNum <= 7 ) {
            return Constants.SECOND_LINE[verseNum - 3] + '\n'
        } 
    }

    stemLines (verseNum) {

        let output = ''
        if (verseNum >= 2 && verseNum <= 7 ) {
            for (let i = verseNum; i > 1; i--) {
                if (i === 3) {
                    output = output + Constants.STEM_ONE + Constants.ANIMALS[ i - 1 ] + 
                         Constants.STEM_TWO + Constants.ANIMALS[ i - 2 ] + ' that ' +
                         Constants.SPIDER
                } else {
                    output = output + Constants.STEM_ONE + Constants.ANIMALS[ i - 1 ] + 
                         Constants.STEM_TWO + Constants.ANIMALS[ i - 2 ]
                }
                output = output + '.\n'
            }
        } else if (verseNum in [1, 8]) {
            output = ''
        }     
                     
        return output          
    }

    closeLine (verseNum) {

        if (verseNum >= 1 && verseNum <= 7) {
            return Constants.FINAL_LINE[0] + '\n'
        } else if (verseNum === 8) {
            return Constants.FINAL_LINE[1] + '\n'
        } 
    } 

    validVerseNum (verseNum) {
        return (Constants.MIN <= verseNum && Constants.MAX >= verseNum) 
    }

    validVerseRange (sVerse, eVerse) {
        return (eVerse >= sVerse && Constants.MIN <= sVerse && Constants.MAX >= sVerse
                && Constants.MIN <= eVerse && Constants.MAX >= eVerse);
    }

    verse (verseNum) {

        var output = '';
        if (this.validVerseNum(verseNum)) {
            output += this.firstLine(verseNum) + this.secondLine(verseNum) + 
                      this.stemLines(verseNum) + this.closeLine(verseNum)
        } else {
            output = 'Verse Number not in the correct range'
        }
        return output
    }

    verses (s, e) {
        
        let output = ''
        if (this.validVerseRange(s, e)) {
            for (let i = s ; i <= e ; i++) {
                output = output + this.verse(i) + '\n'
            }
        } else {
            output = 'Invalid range'
        }
        return output
    }

}

module.exports = FoodChain 