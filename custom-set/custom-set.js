class CustomSet {

    constructor (data = []) {
        this.elements = data
        this.elements = this.elements.filter((e, i) => this.elements.indexOf(e) === i).sort()
    }

    empty () {
        return this.elements.length === 0
    }

    add (element) {
        if (this.elements.indexOf(element) === -1 ) {
            this.elements.push(element)
            this.elements = this.elements.filter((e, i) => this.elements.indexOf(e) === i).sort()
          }
          return this
    }

    contains (element) {
        return this.elements.indexOf(element) >= 0
    }

    eql (other) {
        if (this.elements.length !== other.elements.length) return false
        let eq = true
        for (let i = 0; eq && i < this.elements.length; i++) {
            eq = this.elements[i] === other.elements[i]
        }
        return eq
    }

    intersection (other) {
        const common = this.elements.filter(element => other.elements.indexOf(element) >= 0)
        return new CustomSet(common)
    }

    subset (other) {
        return this.intersection(other).eql(this)
    }

    disjoint (other) {
        return this.intersection(other).empty()
    }

    difference (other) {
        const diffels = this.elements.filter(element => other.elements.indexOf(element) === -1)
        return new CustomSet(diffels)
    }

    union (other) {
        this.elements = this.elements.concat(other.elements)
        this.elements = this.elements.filter((e, i) => this.elements.indexOf(e) === i).sort()
        return this
    }
}

// export default CustomSet
module.exports = CustomSet