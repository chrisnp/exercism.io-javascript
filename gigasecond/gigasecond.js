
class Gigasecond {

    constructor (fromDate) {
        this.fromDate = fromDate
    }

    gigaSec () {
        return 1000000000000
    }

    date () {
        let gigSecDate = new Date(this.fromDate.getTime() + this.gigaSec())
        return gigSecDate
    }

}

module.exports = Gigasecond