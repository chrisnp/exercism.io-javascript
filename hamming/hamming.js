class Hamming {

    constructor () {}

    compute (astr, bstr) {
        let hamm = 0
        if (astr.length === bstr.length) {
           for (var i in astr) { (astr[i] !== bstr[i]) ? hamm += 1 : hamm }
        } else {
           throw Error('DNA strands must be of equal length.');
        } 
        return hamm
    }
    
}

module.exports = Hamming;