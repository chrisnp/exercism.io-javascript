const bracketPush = (str) => {
    let closing
    do {
        closing = str
        str = str.replace(/\[\]|\(\)|\{\}/, '')
    } while (str !== closing && str !== '')
    return str === ''
}

module.exports = bracketPush