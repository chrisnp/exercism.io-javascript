// import { bracketPush } from './bracket-push';
var bracketPush = require('./bracket-push')

describe('bracket push', () => {
  it('checks for appropriate bracketing in a set of brackets', () => {
    expect(bracketPush('{}')).toEqual(true);
  });

  it('returns false for unclosed brackets', () => {
    expect(bracketPush('{{')).toEqual(false);
  });

  it('returns false if brackets are out of order', () => {
    expect(bracketPush('}{')).toEqual(false);
  });

  it('checks bracketing in more than one pair of brackets', () => {
    expect(bracketPush('{}[]')).toEqual(true);
  });

  it('checks bracketing in nested brackets', () => {
    expect(bracketPush('{[]}')).toEqual(true);
  });

  it('rejects brackets that are properly balanced but improperly nested', () => {
    expect(bracketPush('{[}]')).toEqual(false);
  });

  it('checks bracket closure with deeper nesting', () => {
    expect(bracketPush('{[)][]}')).toEqual(false);
  });

  it('checks bracket closure in a long string of brackets', () => {
    expect(bracketPush('{[]([()])}')).toEqual(true);
  });
});
