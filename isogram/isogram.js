class Isogram {
    
    constructor (word) {
        this.word = word
    }

    isIsogram () {
        let w = this.word.toLowerCase().replace(/\s+|-+|_+/g, '')
        for (var e in w) {
            if ((w.match(new RegExp(w[e], "g")) || []).length > 1) { 
                return false
            }
        }
        return true
    }
}

module.exports = Isogram