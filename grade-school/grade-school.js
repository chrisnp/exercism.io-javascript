'use strict'

class School {
    

    constructor () {        
        this.db = {}
    }
    
    sort( grade ) {
        return this.db[grade].sort()
    }

    roster () {
        return this.db
    }

    add ( name, grade ) {
        if (this.db[grade] === undefined) {
            this.db[grade] = []
        }
        this.db[grade].push(name)
        this.sort(grade)
    }

    grade ( grade ) {
        if (this.db[grade] !== undefined) {
            return this.sort(grade)
        } else {
            return this.db[grade] = []
        }

    }

}

module.exports = School