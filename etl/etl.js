class ETL {
    constructor () {
        this.transformed = {}
    }

    transform (obj) {
        this.transformed = {}
        for (var element in obj) {
            obj[element].map(key => key.toLowerCase())
                        .forEach(key => { this.transformed[key] = parseInt(element) })
        }
        return this.transformed
    }
}

module.exports = ETL
